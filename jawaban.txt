no.1) Buatlah database dengan nama “myshop”!
create database myshop;
use myshop;

no.2) Buatlah tabel – tabel baru di dalam database myshop sesuai data-data berikut:
-Table users
create table users(
id INT AUTO_INCREMENT,
name varchar(255),
email varchar(255),
password varchar(255),
PRIMARY KEY(id));

-Table categories
create table categories(
id INT AUTO_INCREMENT,
name varchar(255),
PRIMARY KEY(id));

-Table items
create table items(
id INT AUTO_INCREMENT,
name varchar(255),
description varchar(255),
price INT,
stock INT,
category_id INT,
FOREIGN KEY(category_id) REFERENCES categories(id),
PRIMARY KEY(id));

no.3)Masukkanlah data data berikut dengan perintah SQL “INSERT INTO . . ” ke dalam table yang sudah dibuat pada soal sebelumnya.
-users
insert into users values(' ','John Doe','john@doe.com','john123');
insert into users values(' ','Jane Doe','jane@doe.com','jenita123');

-categories
insert into categories values(' ','gadget');
insert into categories values(' ','cloth');
insert into categories values(' ','men');
insert into categories values(' ','women');
insert into categories values(' ','branded');

-items
insert into items values(' ','Sumsang b50','hape keren dari merek sumsang','4000000','100','1');
insert into items values(' ','Uniklooh','baju keren dari brand ternama','500000','50','2');
insert into items values(' ','IMHO Watch','jam tangan anak yang jujur banget','2000000','10','1');

no.4)Mengambil Data dari Database
a.)Mengambil data users
select id,name,email from users;

b.)Mengambil data items
select id,name,description,stock,category_id,price from items where price >1000000;
select * from items WHERE name LIKE '%uniklo%';

c. Menampilkan data items join dengan kategori
select items.name, items.description, items.price, items.stock, items.category_id, categories.name from categories INNER JOIN items ON categories.id=items.id;

no.5)Mengubah Data dari Database
update items set price='2500000' where name='Sumsang b50';

